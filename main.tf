terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "5.0"
        }
    }
    backend "http" {
    }
}
variable "REPO_URL" {
    type = string
    default = ""
}

provider "aws" {
    region = "us-east-1"
}
resource "aws_amplify_app" "microcloud" {
    name = "microcloud"
    repository = var.REPO_URL
    build_spec = <<-EOT
    version: 0.1
    frontend:
      phases:
        preBuild:
          commands:
            - cd microcloud && npm install
        build:
          commands:
            - cd microcloud && npm run build
      artifacts:
        baseDirectory: microcloud/build
        files:
          - '**/*'
      cache:
        paths:
          - microcloud/node_modules/**/*
    EOT
}